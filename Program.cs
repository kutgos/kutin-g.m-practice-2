﻿using System;

class Program
{
  static void Main(string[] args)
  {
    // создаем объекты классов
    IProductService productService = new ProductService();
    ILogHandler consoleHandler = new ConsoleHandler();
    ILogHandler fileHandler = new FileHandler();
    ILogHandler emailHandler = new EmailHandler();

    // создаем цепочку обработчиков
    LogHandlerChain logHandlerChain = new LogHandlerChain();
    logHandlerChain.AddHandler(consoleHandler);
    logHandlerChain.AddHandler(fileHandler);
    logHandlerChain.AddHandler(emailHandler);

    // создаем объект класса ProductManagement
    IProductManagement productManagement = new ProductManagement(productService, logHandlerChain);

    // создаем продукт
    var computer_builder = new ComputerBuilder();
    var mac_builder = new MacBuilder();

    // Создаем объект директора, который управляет работой строителя
    var director = new Director(computer_builder);

    // Создаем новый компьютер на основе заданных параметров
    director.BuildComputer("Intel Core i7", 16, "Nvidia GeForce RTX 3080", 2000, 750, true);
    var computer = computer_builder.Build();

    director.BuildComputer("Intel Core i7", 16, "Nvidia GeForce RTX 3080", 2000, 750, true);
    var mac = mac_builder.Build();

    // сохраняем продукт
    productManagement.SaveProduct(computer);

    // получаем продукт по ID
    int computerid = 1;
    Computer retrievedProduct = productManagement.GetProductDetails(computerid);

    // удаляем продукт
    productManagement.DeleteProduct(computerid);
  }

}

public class Computer
{
  public string Processor { get; set; }
  public int Ram { get; set; }
  public string VideoCard { get; set; }
  public int HardDrive { get; set; }
  public int PowerSupply { get; set; }
  public bool Cooler { get; set; }
}

public class Mac
{
  public string Processor { get; set; }
  public int Ram { get; set; }
  public string VideoCard { get; set; }
  public int HardDrive { get; set; }
  public int PowerSupply { get; set; }
  public bool Cooler { get; set; }
}


public interface IComputerBuilder
{
  void SetProcessor(string processorName);
  void SetRam(int ramSize);
  void SetVideoCard(string videoCardName);
  void SetHardDrive(int hardDriveSize);
  void SetPowerSupply(int powerSupplyVolume);
  void SetCooler(bool hasCooler);
}

public class ComputerBuilder : IComputerBuilder
{
  private Computer _computer = new Computer();

  public void SetProcessor(string processorName)
  {
    _computer.Processor = processorName;
    Console.WriteLine("Процессор добавлен");
  }

  public void SetRam(int ramSize)
  {
    _computer.Ram = ramSize;
    Console.WriteLine("Оперативная память добавлена");
  }

  public void SetVideoCard(string videoCardName)
  {
    _computer.VideoCard = videoCardName;
    Console.WriteLine("Видеокарта добавлена");
  }

  public void SetHardDrive(int hardDriveSize)
  {
    _computer.HardDrive = hardDriveSize;
    Console.WriteLine("Жесткий диск добавлен");
  }

  public void SetPowerSupply(int powerSupplyVolume)
  {
    _computer.PowerSupply = powerSupplyVolume;
    Console.WriteLine("Объем источника питания добавлен");
  }

  public void SetCooler(bool hasCooler)
  {
    _computer.Cooler = hasCooler;
    Console.WriteLine("Информация о куллере добавлена");
  }

  public Computer Build()
  {
    Console.WriteLine("Компьютер собран" + "\n");
    return _computer;
  }
}

public class MacBuilder : IComputerBuilder
{
  private Mac _mac = new Mac();

  public void SetProcessor(string processorName)
  {
    _mac.Processor = processorName;
    Console.WriteLine("Процессор добавлен");
  }

  public void SetRam(int ramSize)
  {
    _mac.Ram = ramSize;
    Console.WriteLine("Оперативная память добавлена");
  }

  public void SetVideoCard(string videoCardName)
  {
    _mac.VideoCard = videoCardName;
    Console.WriteLine("Видеокарта добавлена");
  }

  public void SetHardDrive(int hardDriveSize)
  {
    _mac.HardDrive = hardDriveSize;
    Console.WriteLine("Жесткий диск добавлен");
  }

  public void SetPowerSupply(int powerSupplyVolume)
  {
    _mac.PowerSupply = powerSupplyVolume;
    Console.WriteLine("Объем источника питания добавлен");
  }

  public void SetCooler(bool hasCooler)
  {
    _mac.Cooler = hasCooler;
    Console.WriteLine("Информация о куллере добавлена");
  }

  public Mac Build()
  {
    Console.WriteLine("Макбук собран" + "\n");
    return _mac;
  }
}

public class Director
{
  private readonly IComputerBuilder _computer_builder;

  public Director(IComputerBuilder computer_builder)
  {
    _computer_builder = computer_builder;
  }

  public void BuildComputer(string processorName, int ramSize, string videoCardName, int hardDriveSize, int powerSupplyVolume, bool hasCooler)
  {
    _computer_builder.SetProcessor(processorName);
    _computer_builder.SetRam(ramSize);
    _computer_builder.SetVideoCard(videoCardName);
    _computer_builder.SetHardDrive(hardDriveSize);
    _computer_builder.SetPowerSupply(powerSupplyVolume);
    _computer_builder.SetCooler(hasCooler);
  }
}

public interface IProductService
{
  Computer GetProduct(int computerid);
  void SaveProduct(Computer computer);
  void DeleteProduct(int computerid);
}

public class ProductService : IProductService
{
  public Computer GetProduct(int computerid)
  {
    Console.WriteLine("Получаем компьютер");
    return new Computer();
  }

  public void SaveProduct(Computer computer)
  {
    Console.WriteLine("Сохраняем компьютер");
  }

  public void DeleteProduct(int computerid)
  {
    Console.WriteLine("Удаляем компьютер");
  }
}

public interface ILogHandler
{
  void SetNext(ILogHandler logHandler);
  void Handle(string message);
  ILogHandler GetNext();
}

public class ConsoleHandler : ILogHandler
{
  private ILogHandler nextHandler;

  public void SetNext(ILogHandler logHandler)
  {
    nextHandler = logHandler;
  }

  public void Handle(string message)
  {
    Console.WriteLine("Логируем в консоль: " + message);

    if (nextHandler != null)
    {
      nextHandler.Handle(message);
    }
  }

  public ILogHandler GetNext()
  {
    return nextHandler;
  }
}

public class FileHandler : ILogHandler
{
  private ILogHandler nextHandler;

  public void SetNext(ILogHandler logHandler)
  {
    nextHandler = logHandler;
  }

  public void Handle(string message)
  {
    Console.WriteLine("Логируем в файл: " + message);

    if (nextHandler != null)
    {
      nextHandler.Handle(message);
    }
  }

  public ILogHandler GetNext()
  {
    return nextHandler;
  }
}

public class EmailHandler : ILogHandler
{
  private ILogHandler nextHandler;

  public void SetNext(ILogHandler logHandler)
  {
    nextHandler = logHandler;
  }

  public void Handle(string message)
  {
    Console.WriteLine("Логируем в почту: " + message);

    if (nextHandler != null)
    {
      nextHandler.Handle(message);
    }
  }

  public ILogHandler GetNext()
  {
    return nextHandler;
  }
}

public class LogHandlerChain
{
  private ILogHandler firstHandler;

  public void AddHandler(ILogHandler handler)
  {
    Console.WriteLine("Добавлен логгер");
    if (firstHandler == null)
    {
      firstHandler = handler;
    }
    else
    {
      ILogHandler currentHandler = firstHandler;

      while (currentHandler != null && currentHandler.GetNext() != null)
      {
        currentHandler = currentHandler.GetNext();
      }

      currentHandler.SetNext(handler);
    }
  }

  public void Handle(string message)
  {
    Console.WriteLine("Запущена цепочка логирования");
    if (firstHandler != null)
    {
      firstHandler.Handle(message);
    }
  }
}

public interface IProductManagement
{
  Computer GetProductDetails(int computerid);
  void SaveProduct(Computer computer);
  void DeleteProduct(int computerid);
}

public class ProductManagement : IProductManagement
{
  private IProductService productService;
  private LogHandlerChain logChain;

  public ProductManagement(IProductService productService, LogHandlerChain logChain)
  {
    this.productService = productService;
    this.logChain = logChain;
  }

  public Computer GetProductDetails(int computerid)
  {
    Computer computer = productService.GetProduct(computerid);

    if (computer != null)
    {
      logChain.Handle("Computer details retrieved");
    }
    else
    {
      logChain.Handle("Product not found: " + computerid);
    }

    return computer;
  }

  public void SaveProduct(Computer computer)
  {
    productService.SaveProduct(computer);
    logChain.Handle("Computer saved");
  }

  public void DeleteProduct(int computerid)
  {
    productService.DeleteProduct(computerid);
    logChain.Handle("Computer deleted: " + computerid);
  }
}
